//#define XERR
#include "state.ih"

size_t State::next1() const
{
//    if (not d_data)
//        xerr("no d_data, object = " << (size_t)this % 1000);

    return d_data ? d_data->next1() : 0;
}

