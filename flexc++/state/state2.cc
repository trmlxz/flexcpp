//#define XERR
#include "state.ih"

State::State(size_t type)
:
    d_type(type),
    d_rule(-1)
{
    xerr((size_t)this % 1000 << ": State(type) -> d_data = 0");
}
